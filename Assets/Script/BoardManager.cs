using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour
{
    public static BoardManager INSTANCE;
    public CellManager PrefabCell;
    public int Width;
    public int Height;
    public int CellSpace;
    public Transform ParentCell;
    private List<CellManager> listCell = new List<CellManager>();
    [Header("Is Random Block")] public bool isRandom = false;
    public int amountBlock;
    [Header("2->24")] [SerializeField] private List<int> listIdCellBlock = new List<int>();
   

    private void Awake()
    {
        if (INSTANCE == null)
        {
            INSTANCE = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void GenerateBoard()
    {
        listCell = new List<CellManager>();
        var totalCell = Width * Height;
        for (var i = 0; i < totalCell; i++)
        {
            var cell = Instantiate(PrefabCell, ParentCell);
            var (x, y) = getXYFromID(i, Width);
            var posCell = new Vector2(x, y);
            cell.AssignData(i, posCell);
            cell.transform.position = new Vector3(x * CellSpace, -y * CellSpace);
            listCell.Add(cell);
        }
        if (isRandom)
        {
            RandomListBlock(amountBlock, totalCell);
        }
        SetCellBlock();
    }

    private void RandomListBlock(int _amountBlock, int _totalCell)
    {
        listIdCellBlock.Clear();
        for (var i = 0; i < _amountBlock; i++)
        {
            var idCellBlock = UnityEngine.Random.Range(2, (_totalCell - 1) / 2);
            while (listIdCellBlock.Contains(idCellBlock))
            {
                idCellBlock = UnityEngine.Random.Range(2, (_totalCell - 1) / 2);
            }
            
            listIdCellBlock.Add(idCellBlock);
        }
    }

    private void SetCellBlock()
    {
        for (var i = 0; i < listIdCellBlock.Count; i++)
        {
            var cell = listCell.FirstOrDefault(x => x.idCell == listIdCellBlock[i]);
            if (cell != null)
            {
                var posCurrCell = cell.GetPosCell();
                var posCellDoiXung = new Vector2((Width - 1) - posCurrCell.x, (Height - 1) - posCurrCell.y);
                var cellDoiXung = listCell.FirstOrDefault(i => i.GetPosCell() == posCellDoiXung);
                cell.SetCellOf(-1);
                cellDoiXung.SetCellOf(-1);
            }
        }
    }

    public (Vector3, Vector3) GetStartAndEndPointBoard()
    {
        var start = listCell[0];
        var end = listCell[listCell.Count - 1];
        return (start.transform.position, end.transform.position);
    }

    public CellManager GetCellInBoard(Vector2 cons)
    {
        return listCell.FirstOrDefault(x => x.pos == cons);
    }

    public (int, int) getXYFromID(int id, int width)
    {
        return (id % width, (int)Mathf.Floor((id / width)));
    }

    public bool IsOnBoard((int, int) newXY)
    {
        if ((newXY.Item1 <= Width - 1 && newXY.Item1 >= 0) && (newXY.Item2 <= Height - 1 && newXY.Item2 >= 0))
        {
            return true;
        }

        return false;
    }
}