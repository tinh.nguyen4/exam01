using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject PrefabPlayer;
    public UISelectedMove PrefabUISelectedMove;
    public Transform tfGui;
    private bool isMoveDone;
    [SerializeField] private UISelectedMove uiSelectedMove;
    [SerializeField] private UISelectedMove uiSelectedMoveEnemy;
    private (int, int) currXYPlayer;
    private GameObject player;
    private int scorePlayer = 0;
    public Text txtScorePlayer;
    private GameObject enemy;
    private CellManager cellEnemyStart;
    private int widthBoard;
    private int heightBoard;

    private void Start()
    {
        CreateUISelectedMove();
        BoardManager.INSTANCE.GenerateBoard();
        widthBoard = BoardManager.INSTANCE.Width - 1;
        heightBoard = BoardManager.INSTANCE.Height - 1;
        GeneratePlayerAndEnemy();
        txtScorePlayer.text = $"Score Player:{scorePlayer}";
    }

    private void OnDisable()
    {
        uiSelectedMove.actionUnRegister(Move);
    }

    private void CreateUISelectedMove()
    {
        uiSelectedMove.actionRegister(Move);
    }


    private void GeneratePlayerAndEnemy()
    {
        var (start, end) = BoardManager.INSTANCE.GetStartAndEndPointBoard();
        player = Instantiate(PrefabPlayer, BoardManager.INSTANCE.transform);
        currXYPlayer = BoardManager.INSTANCE.getXYFromID(0, 7);
        var consPlayer = new Vector2(currXYPlayer.Item1, currXYPlayer.Item2);
        var cellStart = BoardManager.INSTANCE.GetCellInBoard(consPlayer);
        cellStart.SetCellOf(1);
        player.transform.position = start;
        enemy = Instantiate(PrefabPlayer, BoardManager.INSTANCE.transform);
        var consEnemy = new Vector2(BoardManager.INSTANCE.Width - 1, BoardManager.INSTANCE.Height - 1);
        cellEnemyStart = BoardManager.INSTANCE.GetCellInBoard(consEnemy);
        cellEnemyStart.SetCellOf(2);
        enemy.transform.position = end;
        ShowUI();
    }

    private void ShowUI()
    {
        uiSelectedMove.SetOnOff(true);
    }

    public void Move(float dir, int type)
    {
        var newXY = GetNewXY(dir, type);
        if (BoardManager.INSTANCE.IsOnBoard(newXY))
        {
            var cons = new Vector2();
            cons.x = newXY.Item1;
            cons.y = newXY.Item2;
            var cell = BoardManager.INSTANCE.GetCellInBoard(cons);
            if (cell.GetCellOf() == 0)
            {
                currXYPlayer = newXY;
                MoveToTarget(cell, player, 1);
                cell.SetCellOf(1);
            }
        }

        StartCoroutine(MoveEnemy());
    }

    public IEnumerator MoveEnemy()
    {
        yield return new WaitForSeconds(1);
        var cellNext = CheckCellNexEnemy(cellEnemyStart);
        if (cellNext != null && cellNext.GetCellOf() == 0)
        {
            MoveToTarget(cellNext, enemy, 2);
            cellNext.SetCellOf(2);
        }
    }


    private CellManager CheckCellNexEnemy(CellManager _cell)
    {
        CellManager cellNext = new CellManager();
        // new AI 
        var (up, right, down, left) = FindAllDirAvailable(_cell.pos.x, _cell.pos.y);
        var dir = new Vector2();
        dir = FindDirAvailbleCellNext(up, right, down, left);
        cellNext = BoardManager.INSTANCE.GetCellInBoard(dir);
        cellEnemyStart = cellNext;
        return cellNext;
    }

    private Vector2 FindDirAvailbleCellNext(Vector2 _vtDir01, Vector2 _vtDir02, Vector2 _vtDir03, Vector2 _vtDir04)
    {
        var dir = new Vector2();
        var max = 0;
        var (dirTemp1, maxTemp1) = FindDirByNodeMax(_vtDir01, _vtDir02);
        var (dirTemp2, maxTemp2) = FindDirByNodeMax(_vtDir03, _vtDir04);
        dir = CheckDir(maxTemp1, maxTemp2, dirTemp1, dirTemp2);
        return dir;
    }

    private static Vector2 CheckDir(int maxTemp1, int maxTemp2, Vector2 dirTemp1, Vector2 dirTemp2)
    {
        Vector2 dir = new Vector2();
        if (maxTemp1 > maxTemp2)
        {
            dir = dirTemp1;
        }
        else
        {
            dir = dirTemp2;
        }

        return dir;
    }

    private (Vector2, int) FindDirByNodeMax(Vector2 _vt1, Vector2 _vt2)
    {
        var nodeVt1 = CheckNodeFlowDirAvailable(_vt1);
        var nodeVt2 = CheckNodeFlowDirAvailable(_vt2);
        var (dir, maxNode) = (new Vector2(), 0);
        if (nodeVt1 > nodeVt2)
        {
            dir = _vt1;
            maxNode = nodeVt1;
        }
        else if (nodeVt1 < nodeVt2)
        {
            dir = _vt2;
            maxNode = nodeVt2;
        }

        return (dir, maxNode);
    }

    private (Vector2, int) FindDirByNodeMax(int max1, int max2, Vector2 dir1, Vector2 dir2)
    {
        var (dir, max) = (new Vector2(), 0);
        if (max1 > max2)
        {
            dir = dir1;
            max = max1;
        }
        else
        {
            dir = dir2;
            max = max2;
        }

        return (dir, max);
    }

    private int CheckNodeFlowDirAvailable(Vector2 _dir)
    {
        var node = 0;
        var cell = BoardManager.INSTANCE.GetCellInBoard(_dir);
        if (cell != null && cell.GetCellOf() == 0)
        {
            var (up, right, down, left) = FindAllDirAvailable(cell.pos.x, cell.pos.y);
            var cellUp = BoardManager.INSTANCE.GetCellInBoard(up);
            var cellRight = BoardManager.INSTANCE.GetCellInBoard(right);
            var cellDown = BoardManager.INSTANCE.GetCellInBoard(down);
            var cellLeft = BoardManager.INSTANCE.GetCellInBoard(left);
            if (cellUp != null && cellUp.GetCellOf() == 0)
            {
                node++;
            }

            if (cellRight != null && cellRight.GetCellOf() == 0)
            {
                node++;
            }

            if (cellDown != null && cellDown.GetCellOf() == 0)
            {
                node++;
            }

            if (cellLeft != null && cellLeft.GetCellOf() == 0)
            {
                node++;
            }
        }

        return node;
    }

    private (Vector2, Vector2, Vector2, Vector2) FindAllDirAvailable(float currX, float curry)
    {
        var (up, right, down, left) = (new Vector2(), new Vector2(), new Vector2(), new Vector2());

        up = new Vector2(currX, curry - 1);
        right = new Vector2(currX + 1, curry);
        down = new Vector2(currX, curry + 1);
        left = new Vector2(currX - 1, curry);
        return (up, right, down, left);
    }


    private List<CellManager> CheckCellFlowDir(float _currX, float _currY, EnemyDir _dirEnemy)
    {
        var listTemp = new List<CellManager>();
        switch (_dirEnemy)
        {
            case EnemyDir.UP:
                while (_currY >= 0)
                {
                    _currY--;
                    var cell = BoardManager.INSTANCE.GetCellInBoard(new Vector2(_currX, _currY));
                    if (cell == null || cell.GetCellOf() != 0)
                    {
                        break;
                    }

                    listTemp.Add(cell);
                }

                break;
            case EnemyDir.RIGHT:
                while (_currX <= widthBoard)
                {
                    _currX++;
                    var cell = BoardManager.INSTANCE.GetCellInBoard(new Vector2(_currX, _currY));
                    if (cell == null || cell.GetCellOf() != 0)
                    {
                        break;
                    }

                    listTemp.Add(cell);
                }

                break;
            case EnemyDir.DOWN:
                while (_currY <= heightBoard)
                {
                    _currY++;
                    var cell = BoardManager.INSTANCE.GetCellInBoard(new Vector2(_currX, _currY));
                    if (cell == null || cell.GetCellOf() != 0)
                    {
                        break;
                    }

                    listTemp.Add(cell);
                }

                break;
            case EnemyDir.LEFT:
                while (_currX >= 0)
                {
                    _currX--;
                    var cell = BoardManager.INSTANCE.GetCellInBoard(new Vector2(_currX, _currY));
                    if (cell == null || cell.GetCellOf() != 0)
                    {
                        break;
                    }

                    listTemp.Add(cell);
                }

                break;
        }

        return listTemp;
    }

    private void MoveToTarget(CellManager _cell, GameObject owner, int type)
    {
        var posPlayer = owner.transform.position;
        while (Vector3.Distance(_cell.transform.position, posPlayer) > 0)
        {
            owner.transform.position = Vector3.MoveTowards(posPlayer, _cell.transform.position, 5 * Time.deltaTime);
            posPlayer = owner.transform.position;
        }
    }

    private (int, int) GetNewXY(float dir, int type)
    {
        var (x, y) = currXYPlayer;
        switch (type)
        {
            case 0:
                y += (int)dir;
                break;
            case 1:
                x += (int)dir;
                break;
            case 2:
                y += (int)dir;
                break;
            case 3:
                x += (int)dir;
                break;
        }

        return (x, y);
    }
}

public enum EnemyDir
{
    NONE,
    UP,
    RIGHT,
    DOWN,
    LEFT
}