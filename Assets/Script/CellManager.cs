using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellManager : MonoBehaviour
{
    public Vector2 pos;
    public int idCell;
    private int cellOf; // 0: empty , 1: Player, 2: enemy
    private SpriteRenderer spriteRenderer;
    private Color clrEmpty = Color.white;
    private Color clrPlayer = Color.blue;
    private Color clrEnemy = Color.red;
    private Color clrBlock = Color.grey;


    private void Awake()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    public void AssignData(int id, Vector2 posCell)
    {
        idCell = id;
        pos = posCell;
    }

    public Vector2 GetPosCell()
    {
        return pos;
    }
    public void SetCellOf(int _cellOf)
    {
        cellOf = _cellOf;
        SetColorCell(cellOf);
    }

    public int GetCellOf()
    {
        return cellOf;
    }

    private void SetColorCell(int _cellOf)
    {
        switch (_cellOf)
        {
            case -1:
                spriteRenderer.color = clrBlock;
                break;
            case 0:
                spriteRenderer.color = clrEmpty;
                break;
            case 1:
                spriteRenderer.color = clrPlayer;
                break;
            case 2:
                spriteRenderer.color = clrEnemy;
                break;
        }
    }
}